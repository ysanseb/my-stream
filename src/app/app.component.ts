import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Genre, Movie } from './shared/models/movie.model';
import { MovieService } from './shared/services/movie.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public genres: Genre[] = [];
  public topTen: Movie[] = [];
  public movies: Movie[] = [];

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    forkJoin({
      genreList: this.movieService.getGenres(),
      firstPage: this.movieService.getMovies(1),
      secondPage: this.movieService.getMovies(2),
    })
      .subscribe(({ genreList, firstPage, secondPage }) => {
        this.genres = genreList.genres;
        this.topTen = [...firstPage.results, ...secondPage.results].slice(0, 10)
        this.movies = [...firstPage.results, ...secondPage.results].slice(10, 40)        
      });

      
  }




}






