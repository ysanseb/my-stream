import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './components/slider/slider.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MovieCardComponent } from './components/movie-card/movie-card.component';



@NgModule({
  declarations: [
    SliderComponent,
    MovieCardComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule
  ],
  exports: [
    SliderComponent,
    MovieCardComponent
  ]
})
export class SharedModule { }
