import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Genres, Results } from '../models/movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private apiKey = 'c89646cb9c2f9f7a6144c074fff0e9c7';
  
  constructor(private http: HttpClient) {}

  getMovies(page: number): Observable<Results> {
    return this.http
      .get<Results>(`https://api.themoviedb.org/3/movie/popular?api_key=${this.apiKey}&page=${page}`);
  }

  getGenres(): Observable<Genres> {
    return this.http
      .get<Genres>(`https://api.themoviedb.org/3/genre/movie/list?api_key=${this.apiKey}`);
  }

}
