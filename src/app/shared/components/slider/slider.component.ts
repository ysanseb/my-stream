import { Component, Input } from '@angular/core';
import { Genre, Movie } from '../../models/movie.model';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent {
  @Input() movies:  Movie[] = [];
  @Input() genres:  Genre[] = [];

  getGenre(id: number): string {
    let genre = this.genres.find(genre => genre.id === id);
    return genre ? genre.name : 'error';
  }
}
